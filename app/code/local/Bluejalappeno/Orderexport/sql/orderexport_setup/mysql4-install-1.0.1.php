<?php

$installer = $this;
$installer->startSetup();

$this->run("

DROP TABLE IF EXISTS `{$installer->getTable('orderexport_export')}`;
CREATE TABLE {$installer->getTable('orderexport_export')} (
  `order_id` int(10) unsigned NOT NULL,
  `exported` int(6) NOT NULL default 0,
   PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");
if  (Mage::helper('bjalcommon')->getNewVersion() >= 8 ) {
	$installer->getConnection()
		->addColumn($installer->getTable('sales/order_grid'), 'exported', "int(6) not null default 0");

	$installer->getConnection()->addKey($installer->getTable('sales/order_grid'),   'exported',    'exported');
}


$installer->endSetup();