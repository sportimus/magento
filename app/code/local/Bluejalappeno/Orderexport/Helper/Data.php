<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Orderexport_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function setExported($orderId) {
		$model=Mage::getModel('orderexport/exported')
			->setData('order_id' ,$orderId)
			->setData('exported','1')
		     ->save();
		if  (Mage::helper('bjalcommon')->getNewVersion() >= 8 ) {
			Mage::getResourceModel('orderexport/exported')->setGridExported($orderId);
		}
	}

	public function isExported($orderId) {

		$exportedCollection = Mage::getModel('orderexport/exported')->getCollection()
		->addOrderFilter($orderId);
		$exportedCollection->load();
		if (count($exportedCollection) > 0) {
			return true;
		}
		return false;
	}
}