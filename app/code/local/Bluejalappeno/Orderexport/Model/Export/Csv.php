<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Orderexport_Model_Export_Csv extends Bluejalappeno_Orderexport_Model_Export_Abstractcsv
{
	const ENCLOSURE = '"';
	const DELIMITER = ',';

	/**
	 * Concrete implementation of abstract method to export given orders to csv file in var/export.
	 *
	 * @param $orders List of orders of type Mage_Sales_Model_Order or order ids to export.
	 * @return String The name of the written csv file in var/export
	 */
	public function exportOrders($orders)
	{
		$fileName = 'order_export_'.date("Ymd_His").'.csv';
		$fp = fopen(Mage::getBaseDir('export').'/'.$fileName, 'w');
		$this->writeHeadRow($fp);
		foreach ($orders as $orderId) {
			$order = Mage::getModel('sales/order')->load($orderId);
			$this->writeOrder($order, $fp);
			Mage::helper('orderexport')->setExported($orderId);
		}
		fclose($fp);
		return $fileName;
	}

	/**
	 * Writes the head row with the column names in the csv file.
	 *
	 * @param $fp The file handle of the csv file
	 */
	protected function writeHeadRow($fp)
	{
		$this->fputcsv($fp, $this->getHeadRowValues(), self::DELIMITER, self::ENCLOSURE);
	}

	/**
	 * Writes the row(s) for the given order in the csv file.
	 * A row is added to the csv file for each ordered item.
	 *
	 * @param Mage_Sales_Model_Order $order The order to write csv of
	 * @param $fp The file handle of the csv file
	 */
	protected function writeOrder($order, $fp)
	{
		$common = $this->getCommonOrderValues($order);

		$orderItems = $order->getItemsCollection();

		if(!Mage::getStoreConfig('order_export/export_orders/prod_attributes')){
			$products = array();

			foreach($orderItems as $item){
				if (!$item->isDummy()) {
					if ($this->getItemOptions($item) != '') {
						$products[] = $this->decodeString($item->getName()).' - ' .$this->decodeString($this->getItemOptions($item));
					}
					else {
						$products[] = $this->decodeString($item->getName());
					}

				}
			}

			$productsOrdered = implode(",",$products);

			array_push($common, $productsOrdered);

			$this->fputcsv($fp, $common, self::DELIMITER, self::ENCLOSURE);
		} else{
			$itemInc = 0;

			foreach ($orderItems as $item)
			{

				if (!$item->getHasChildren()) {

					$record = array_merge($common, $this->getOrderItemValues($item, $order, ++$itemInc));
					$this->fputcsv($fp, $record, self::DELIMITER, self::ENCLOSURE);
				}
				//if item is a bundle with fixed price then we need to show parent item as well in itemised export
				else if ($item->getHasChildren() && !$item->isChildrenCalculated() && $item->getData('product_type') == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
					$parentRecord = array_merge($common, $this->getOrderItemValues($item, $order, ++$itemInc));
					$this->fputcsv($fp, $parentRecord, self::DELIMITER, self::ENCLOSURE);
				}

			}


		}

	}






	/**
	 * Returns the head column names.
	 *
	 * @return Array The array containing all column names
	 */
	protected function getHeadRowValues()
	{
		$basicHeadings = array(
            'Order Number',
            'Order Date',
    		'Date Shipped',
            'Order Status',
            'Order Purchased From',
            'Order Payment Method',
    		'Order Currency',
    		'Credit Card Type',
            'Order Shipping Method',
    		'Order Coupon Code',
            'Order Subtotal',
            'Order Tax',
            'Order Shipping',
            'Order Discount',
            'Order Grand Total',
            'Order Base Grand Total',
        	'Order Paid',
            'Order Refunded',
            'Order Due',
            'Total Qty Items Ordered',
    		'Order Weight',
    		'Order Gift Message',
            'Customer Name',
            'Customer Email',
    		'Customer Group',
    		'Customer Tax/VAT Number',
            'Shipping Name',
            'Shipping Company',
            'Shipping Street',
            'Shipping Zip',
            'Shipping City',
        	'Shipping State',
            'Shipping State Name',
            'Shipping Country',
            'Shipping Country Name',
            'Shipping Phone Number',
    		'Billing Name',
            'Billing Company',
            'Billing Street',
            'Billing Zip',
            'Billing City',
        	'Billing State',
            'Billing State Name',
            'Billing Country',
            'Billing Country Name',
            'Billing Phone Number');

		$orderAttSetting = Mage::getStoreConfig('order_export/export_orders/add_order_att');
		$order_attributes = explode(',', $orderAttSetting);
		$keysToRemove = array_keys($order_attributes,'');
		foreach($keysToRemove as $k) {
		    unset($order_attributes[$k]);
		}
		if(count($order_attributes)> 0 && $order_attributes[0] != '') {
			$basicHeadings = array_merge($basicHeadings, $order_attributes);
		}

		if(Mage::getStoreConfig('order_export/export_orders/prod_attributes')){
			$headings =  array(
            'Order Item Increment',
    		'Item Name',
            'Item Status',
       		'Item Product Type',
            'Item SKU',
            'Item Options',
            'Item Original Price',
    		'Item Price',
       		'Item Base Price',
            'Item Qty Ordered',
        	'Item Qty Invoiced',
        	'Item Qty Shipped',
        	'Item Qty Canceled',
            'Item Qty Refunded',
            'Item Tax',
            'Item Discount',
    		'Item Total',
       		'Item Base Total',
       		'Item Base Row Total',
       		'Categories',
       		'Item URL',
       		'Item Gift Message');
			$headings = array_merge($basicHeadings, $headings);
			$attributeSetting = Mage::getStoreConfig('order_export/export_orders/add_attributes');
			$product_attributes = explode(',', $attributeSetting);
			$keysToRemove = array_keys($product_attributes,'');
			foreach($keysToRemove as $k) {
			    unset($product_attributes[$k]);
			}
			$headings = array_merge($headings, $product_attributes);
			return $headings;
		}else{
			array_push($basicHeadings,'Order Items');
			return $basicHeadings;
		}

	}

	/**
	 * Returns the values which are identical for each row of the given order. These are
	 * all the values which are not item specific: order data, shipping address, billing
	 * address and order totals.
	 *
	 * @param Mage_Sales_Model_Order $order The order to get values from
	 * @return Array The array containing the non item specific values
	 */
	protected function getCommonOrderValues($order)
	{
		$shippingAddress = !$order->getIsVirtual() ? $order->getShippingAddress() : null;
		$billingAddress = $order->getBillingAddress();
		$newBillingStreet = $this->getStreet($billingAddress);
		$commonValues = array();

		if (Mage::getStoreConfig('order_export/export_orders/sage_base')) {

			$subtotal = $this->formatBasePrice($order->getData('base_subtotal'), $order);
			$taxAmount = $this->formatBasePrice($order->getData('base_tax_amount'), $order);
			$shipping = $this->formatBasePrice($order->getData('base_shipping_amount'), $order);
			$discount = $this->formatBasePrice($order->getData('base_discount_amount'), $order);
			$grandTotal = $this->formatBasePrice($order->getData('base_grand_total'), $order);
			$totalPaid = $this->formatBasePrice($order->getData('base_total_paid'), $order);
			$totalRefunded = $this->formatBasePrice($order->getData('base_total_refunded'), $order);
			$totalDue = $this->formatBasePrice($order->getData('base_total_due'), $order);
		}
		else {
			$subtotal = $this->formatPrice($order->getData('subtotal'), $order);
			$taxAmount = $this->formatPrice($order->getData('tax_amount'), $order);
			$shipping = $this->formatPrice($order->getData('shipping_amount'), $order);
			$discount = $this->formatPrice($order->getData('discount_amount'), $order);
			$grandTotal = $this->formatPrice($order->getData('grand_total'), $order);
			$totalPaid = $this->formatPrice($order->getData('total_paid'), $order);
			$totalRefunded = $this->formatPrice($order->getData('total_refunded'), $order);
			$totalDue = $this->formatPrice($order->getData('total_due'), $order);
		}
		$pcode = $shippingAddress ? $shippingAddress->getData("postcode") : '';
		$customerGroup = Mage::getModel('customer/group')->load((int)$order->getCustomerGroupId())->getCode();
		$vatNumber = $order->getCustomerTaxvat();
		$giftMessage = Mage::getModel('giftmessage/message')->load($order->getGiftMessageId())->getMessage();
		$shippedDate = '';
		if($order->hasShipments())
		{
			$shipments = $order->getShipmentsCollection();
			foreach($shipments as $shipment)
			{
				$shippedDate = Mage::helper('core')->formatDate($shipment->getCreatedAt(), 'medium', true);
				break;
			}
		}

		$commonValues = array(
		$order->getRealOrderId(),
		Mage::helper('core')->formatDate($order->getCreatedAt(), 'medium', true),
		$shippedDate,
		$order->getStatus(),
		$this->decodeString($this->getStoreName($order)),
		$this->getPaymentMethod($order),
		$order->getOrderCurrencyCode(),
		$this->getCcType($order),
		$this->decodeString($this->getShippingMethod($order)),
		$this->decodeString($order->getCouponCode()),
		$subtotal,
		$taxAmount,
		$shipping,
		$discount,
		$grandTotal,
		$this->formatBasePrice($order->getData('base_grand_total'), $order),
		$totalPaid,
		$totalRefunded,
		$totalDue,
		$this->getTotalQtyItemsOrdered($order),
		$order->getWeight(),
		$giftMessage,
		$this->decodeString($order->getCustomerName()),
		$order->getCustomerEmail(),
		$customerGroup,
		$vatNumber,
		$shippingAddress ? $this->decodeString($shippingAddress->getName()) : '',
		$shippingAddress ? $this->decodeString($shippingAddress->getData("company")) : '',
		$shippingAddress ? $this->decodeString($this->getStreet($shippingAddress)) : '',
		$pcode,//$shippingAddress ? $shippingAddress->getData("postcode") : '',
		$shippingAddress ? $this->decodeString($shippingAddress->getData("city")) : '',
		$shippingAddress ? $this->decodeString($shippingAddress->getRegionCode()) : '',
		$shippingAddress ? $this->decodeString($shippingAddress->getRegion()) : '',
		$shippingAddress ? $shippingAddress->getCountry() : '',
		$shippingAddress ? $this->decodeString($shippingAddress->getCountryModel()->getName()) : '',
		$shippingAddress ? $shippingAddress->getData("telephone") : '',
		$this->decodeString($billingAddress->getName()),
		$this->decodeString($billingAddress->getData("company")),
		$this->decodeString($this->getStreet($billingAddress)),
		$billingAddress->getData("postcode"),
		$this->decodeString($billingAddress->getData("city")),
		$this->decodeString($billingAddress->getRegionCode()),
		$this->decodeString($billingAddress->getRegion()),
		$billingAddress->getCountry(),
		$this->decodeString($billingAddress->getCountryModel()->getName()),
		$billingAddress->getData("telephone")
		);
		if (($attributeSetting = Mage::getStoreConfig('order_export/export_orders/add_order_att')) != '') {
			$order_attributes = explode(',', $attributeSetting);
			if (count($order_attributes) > 0) {
				$amastyOrdAttr = Mage::helper('bjalcommon')->isModuleEnabled('Amasty_Orderattr');
	    		foreach ($order_attributes as $attribute_code) {
	    			if($amastyOrdAttr && $order->custom($attribute_code) != '') {
	    				array_push($commonValues, (string)$order->custom($attribute_code));
	    			}
	    			elseif(!is_array($order->getData($attribute_code)) && (string)$order->getAttributeText($attribute_code) != '') {
						array_push($commonValues, (string)$order->getAttributeText($attribute_code));
					}
					else {
						if (is_array($order->getData($attribute_code)) || is_object($order->getData($attribute_code))) {
							array_push($commonValues,  print_r($order->getData($attribute_code), true));
						}
						else
						{
							array_push($commonValues, $this->decodeString((string)$order->getData($attribute_code)));
						}
					}
				}
			}
		}
		return $commonValues;
	}

	/**
	 * Returns the item specific values.
	 *
	 * @param Mage_Sales_Model_Order_Item $item The item to get values from
	 * @param Mage_Sales_Model_Order $order The order the item belongs to
	 * @return Array The array containing the item specific values
	 */
	protected function getOrderItemValues($item, $order, $itemInc=1)
	{
		//if item is configurable then we need to get prices from the parent object
		if (!$item->isChildrenCalculated() && $item->getParentItem() && $item->getParentItem()->getData('product_type') == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
			$priceItem = $item->getParentItem();
		}
		else {
			$priceItem = $item;
		}
		if (Mage::getStoreConfig('order_export/export_orders/sage_base')) {
			$origPrice =  $this->formatBasePrice($priceItem->getData('base_original_price'), $order);
			$price =  $this->formatBasePrice($priceItem->getData('base_price'), $order);
			$tax =  $this->formatBasePrice($priceItem->getData('base_tax_amount'), $order);
			$discount =  $this->formatBasePrice($priceItem->getData('base_discount_amount'), $order);
			$itemTotal =  $this->formatBasePrice($this->getBaseItemTotal($priceItem), $order);
		}
		else {
			$origPrice = $this->formatPrice($priceItem->getData('original_price'), $order);
			$price = $this->formatPrice($priceItem->getData('price'), $order);
			$tax = $this->formatPrice($priceItem->getData('tax_amount'), $order);
			$discount = $this->formatPrice($priceItem->getData('discount_amount'), $order);
			$itemTotal = $this->formatPrice($this->getItemTotal($priceItem), $order);
		}

		$product = Mage::getModel('catalog/product')->load($item->getProductId());
		$categoryString = '';
		foreach ($product->getCategoryIds() as $categoryId) {
			$category = Mage::getModel('catalog/category')->load($categoryId);
			$categoryString .= $this->decodeString($category->getName()) .' ,';
		}
		$categoryString = substr($categoryString, 0, -1);
		$giftMessageItem =  Mage::getSingleton('giftmessage/message')->load($item->getGiftMessageId())->getMessage();

		$values = array(
		$itemInc,
		$this->decodeString($item->getName()),
		$priceItem->getStatus(),
		$item->getData('product_type'),
		$this->decodeString($this->getItemSku($item)),
		$this->decodeString($this->getItemOptions($priceItem)),
		$origPrice,
		$price,
		$this->formatBasePrice($priceItem->getData('base_price'), $order),
		(int)$item->getQtyOrdered(),
		(int)$item->getQtyInvoiced(),
		(int)$priceItem->getQtyShipped(),
		(int)$item->getQtyCanceled(),
		(int)$item->getQtyRefunded(),
		$tax,
		$discount,
		$itemTotal,
		$this->formatBasePrice($this->getBaseItemTotal($priceItem), $order),
		$this->formatBasePrice($priceItem->getBaseRowTotal(), $order),
		$categoryString,
		str_replace('index.php' , '' , Mage::getUrl($product->getUrlPath())),
		$giftMessageItem

		);
		return array_merge($values, $this->getAttributeValues($item, $order));
	}

	protected function getAttributeValues($item, $order) {
		$values = array ();
		if (($attributeSetting = Mage::getStoreConfig('order_export/export_orders/add_attributes')) != '') {
			$product_attributes = explode(',', $attributeSetting);
			if (count($product_attributes) > 0) {
				$product = Mage::getModel('catalog/product')->load($item->getProductId());
				foreach ($product_attributes as $attribute_code) {
					if($attribute_code == '') {
						continue;
					}
					if (!is_array($product->getData($attribute_code)) && !is_array($product->getAttributeText($attribute_code)) && (string)$product->getAttributeText($attribute_code) != '' ) {
						array_push($values, (string)$product->getAttributeText($attribute_code));
					}
					else {
						if (is_array($product->getData($attribute_code)) || is_object($product->getData($attribute_code))) {
							array_push($values,  print_r($product->getData($attribute_code), true));
						}
						else
						{
							array_push($values, $this->decodeString((string)$product->getData($attribute_code)));
						}
					}
				}
			}
		}
		return $values;
	}
}
?>