<?php

/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Orderexport_Model_Options_Orderattributes
{

	/**
     * Default ignored attribute codes
     *
     * @var array
     */
    protected $_ignoredAttributeCodes = array(
        'custom_design','custom_design_from','custom_design_to','custom_layout_update',
        'gift_message_available','news_from_date','news_to_date','options_container',
        'price_view','sku_type','image_label', 'thumbnail_label','name', 'status', 'sku' ,'price' ,
        'enable_googlecheckout', 'small_image_label'
    );

    /**
     * Default ignored attribute types
     *
     * @var array
     */
    protected $_ignoredAttributeTypes = array('hidden', 'media_image', 'image', 'gallery');

	public function toOptionArray()
    {

           //Returns only the bespoke Order Attributes
   		$entityTypeId =  Mage::getModel('eav/config')->getEntityType('order')->getEntityTypeId();
    	$collection = Mage::getResourceModel('eav/entity_attribute_collection');
    	$collection->setEntityTypeFilter($entityTypeId);
    	$attributes = array();
        foreach ($collection as $attribute) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute */
   	//	if ($this->_isAllowedAttribute($attribute)) {
	           $attributes[] = array(
	                'value' => $attribute->getAttributeCode(),
	                'label' => $attribute->getFrontend()->getLabel(),
	           );
        //	}
        }

        //returns every column in the database
    /*	$collection = Mage::getResourceModel('orderexport/exported')->getOrderColumns();
    	foreach ($collection as $key => $value) {
                $attributes[] =  array(
	                'value' => $value,
	                'label' => $value,
	           );
        }*/
    	array_unshift($attributes, array('value'=>'', 'label'=>Mage::helper('catalog')->__('Please select ...')));
       return $attributes;

    }


 	protected function _isAllowedAttribute($attribute)
    {
        return !in_array($attribute->getFrontendInput(), $this->_ignoredAttributeTypes)
               && !in_array($attribute->getAttributeCode(), $this->_ignoredAttributeCodes)
               && $attribute->getFrontendLabel() != "";
    }
}


