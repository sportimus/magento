<?php

/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */
class Bluejalappeno_Orderexport_Model_Options_Exported
{
	public function toOptionArray()
    {
        return array(0 => Mage::helper('adminhtml')->__('No'),
            1 => Mage::helper('adminhtml')->__('Yes')
        );
    }

}