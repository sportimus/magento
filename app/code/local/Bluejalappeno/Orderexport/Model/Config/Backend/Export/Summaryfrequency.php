<?php

/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Orderexport_Model_Config_Backend_Export_Summaryfrequency
{
     const CRON_DAILY    = 'D';
    const CRON_WEEKLY   = 'W';
    const CRON_MONTHLY  = 'M';
    const CRON_UNUSED  = 'ZZ';
    
 public function toOptionArray()
    {
        return array(
        	array('value' => self::CRON_UNUSED, 'label'=>Mage::helper('orderexport')->__('Not required')),
            array('value'=>self::CRON_DAILY, 'label'=>Mage::helper('orderexport')->__('Daily')),
            array('value'=>self::CRON_WEEKLY, 'label'=>Mage::helper('orderexport')->__('Weekly')),
            array('value'=>self::CRON_MONTHLY, 'label'=>Mage::helper('orderexport')->__('Monthly')),
        );
    }
}
    