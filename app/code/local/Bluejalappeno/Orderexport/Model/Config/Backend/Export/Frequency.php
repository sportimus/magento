<?php

/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Orderexport
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Orderexport_Model_Config_Backend_Export_Frequency
{
  const CRON_FIVEMINS    = 'F';
    const CRON_HOURLY    = 'H';
     const CRON_DAILY    = 'D';
    const CRON_WEEKLY   = 'W';
    const CRON_MONTHLY  = 'M';
    
 public function toOptionArray()
    {
        return array(
            array('value'=>self::CRON_FIVEMINS, 'label'=>Mage::helper('sitemap')->__('Every 5 minutes')),
            array('value'=>self::CRON_HOURLY, 'label'=>Mage::helper('sitemap')->__('Hourly')),
            array('value'=>self::CRON_DAILY, 'label'=>Mage::helper('sitemap')->__('Daily')),
            array('value'=>self::CRON_WEEKLY, 'label'=>Mage::helper('sitemap')->__('Weekly')),
            array('value'=>self::CRON_MONTHLY, 'label'=>Mage::helper('sitemap')->__('Monthly')),
        );
    }
}
    