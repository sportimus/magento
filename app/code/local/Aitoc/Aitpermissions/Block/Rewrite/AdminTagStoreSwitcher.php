<?php
/**
 * Advanced Permissions
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitpermissions
 * @version      2.10.6
 * @license:     Lks8VRRO2OBZ5t1oqQiLltPWAJmbCHxjjpjrTeuA9N
 * @copyright:   Copyright (c) 2015 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitpermissions_Block_Rewrite_AdminTagStoreSwitcher
    extends Aitoc_Aitpermissions_Block_Rewrite_AdminStoreSwitcher
{
    public function __construct()
    {
        parent::__construct();
        $this->setUseConfirm(false)->setSwitchUrl(
            $this->getUrl('*/*/*/', array('store' => null, '_current' => true))
        );
    }
}